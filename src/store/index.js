import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  //状态：要交互的数据
  state: {
    goodsList: [], //商品列表
    sellerList: [], //商家的店铺信息
  },
  // 改变：改变state值的唯一方式
  mutations: {
    // 1.接收order.vue传来 初始化获取商品数据列表
    initgoodsList(state, newarr) {
      state.goodsList = newarr
    },
    // 2.接收Nav.vue传来 初始化获取商家数据列表
    initsellerList(state, newarr) {
      state.sellerList = newarr
    },

    // 接收order.vue传来 改变购物车的商品数量
    changeNum(state, params) {
      // 把值+1或者-1 因为goodsList里面结果是[{[foods,name]},{}]
      for (let list of state.goodsList) {
        for (let child of list.foods) {
          if (child.id == params.id) {//对应选中
            child.num += params.num;//让商品数量+1/-1
            return//提高代码的性能

          }

        }
      }

    }
  },
  // 计算属性，用法作用同computed，母体数据不变，不会重复运算（区别：computed是vue的属性，getters是vuex的属性）
  getters: {
    // 购物车数据，筛选数量>0的放进小黄车
    shopCarList(state) {
      let arr = [];
      for (let obj of state.goodsList) {
        for (let child of obj.foods) {
          if (child.num > 0) {
            arr.push(child);//把商品数量大于0的放进购物车
          }
        }
      }
      return arr;
    }
  },

})

import Vue from 'vue'
import VueRouter from 'vue-router'
import Nav from '../views/Nav'
Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    redirect: '/nav/order'  //默认用户输入/就可以访问 /nav/order这个页面来
  },
  {
    path: '/nav',
    name: 'Nav',
    component: Nav,
    //二级路由
    children: [
      {
        path: '/nav/order',
        name: '/nav/order',
        component: () => import('../views/nav/order.vue')
      },
      {
        path: '/nav/evaluation',
        name: '/nav/order',
        component: () => import('../views/nav/evaluation.vue')
      },
      {
        path: '/nav/merchant',
        name: '/nav/order',
        component: () => import('../views/nav/merchant.vue')
      },

    ]
  },
  // 一级路由
  {
    path: '/goods',
    name: 'Goods',
    component: () => import('../views/Goods.vue')
  },


]
const router = new VueRouter({
  routes
})
export default router

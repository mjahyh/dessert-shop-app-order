# 甜品店APP点餐
## 项目介绍
项目技术栈：Vue.js + vue-router + vuex + axios + Vant +echarts；父子路由，路由传参。 甜品店移动端点餐系统，用户可以在此平台上进行下单，实现消费。
该项目，根据另外一套后台管理系统连动起来，[查看另外一个网站（甜品店后台管理系统）](https://gitee.com/abc_1326503055/order-management.git)，该项目包含PC端和移动端，两者实现完整的一套外卖点餐系统。
## 使用说明

1. 需要提前安装好 nodejs 与 yarn,下载项目后在项目主目录下运行 yarn 拉取依赖包。
2. 运行后台服务器，打开sell-serve文件，在控制台输入yarn serve 启动后台数据。
3. 启动项目：直接使用命令 yarn serve或npm run serve。
4. 项目打包 ：yarn bulid

## Vue-cli搭建开发环境
### 搭建项目架构：
项目采用Webpack+Vue-router的架构方式，开始安装（windows系统上操作）
1. 配置npm的淘宝镜像
    npm config set registry https://registry.npmjs.org/
2. 下载安装配置yarn
-   2.1 npm i yarn -g
-   2.2 配置path环境变量
-       yarn global bin //查看全局安装目录,把目录粘贴到系统环境变量PATH中(增量更新,不要覆盖之前的)
-   2.3 配置yarn淘宝镜像
-       yarn config set registry http://registry.npm.taobao.org/

			
3. 安装vue脚手架,创建项目  
-  3.1 下载脚手架
-     yarn global add @vue/cli
-  3.2 使用脚手架创建项目
-     vue create 项目名(不能中文)
-  3.3 启动运行项目
-     yarn serve



## 商品点餐功能
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E5%9B%BE/%E5%95%86%E5%93%81.PNG)
## 购物车功能
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E5%9B%BE/%E8%B4%AD%E7%89%A9%E8%BD%A6.PNG)
## 评价功能
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E5%9B%BE/%E8%AF%84%E4%BB%B7.PNG)
## 商家功能
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E5%9B%BE/%E5%95%86%E5%AE%B6.PNG)
## 商品详情页
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E5%9B%BE/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85%E9%A1%B5.PNG)

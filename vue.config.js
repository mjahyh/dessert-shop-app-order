// Vue.config.js 配置选项

module.exports = {

    // 配置 webpack-dev-server 行为。
    devServer: {
        open: true,
        host: 'localhost',
        port: 8081,
        https: false,
        hotOnly: false,

        // 配置代理服务器
        proxy: {
            '/api': {
                target: 'http://ip:port',  // 请求的第三方接口
                secure: false,   //如果是http接口，需要配置该参数
                ws: true,  // proxy websockets，访问网关，使用http的连接方式进行socket信息推送
                changeOrigin: true,  // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
                pathRewrite: {  // 路径重写，
                    '^/api': ''  // 替换target中的请求地址，也就是说以后你在请求http://ip:port这个地址的时候直接写成/api即可。
                }
            }
        },

        before: app => { }
    },




}
